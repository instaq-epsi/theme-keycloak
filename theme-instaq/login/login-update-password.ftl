<#import "template.ftl" as layout>
<@layout.registrationLayout displayInfo=true; section>
    <#if section = "form">
        <form id="kc-passwd-update-form" class="form" action="${url.loginAction}" method="post">
            <input type="text" id="username" name="username" value="${username}" autocomplete="username" readonly="readonly" style="display:none;"/>
            <input type="password" id="password" name="password" autocomplete="current-password" style="display:none;"/>

            <label class="prettyinput input">
                <input type="password" id="password-new" name="password-new" required placeholder=" " autofocus>
                <span class="label">
                    ${msg("passwordNew")}
                </span>
            </label>
            <div class="errors" id="password-error">
                <div class="error">
                    ${msg("passwordError")}
                </div>
            </div>


            <label class="prettyinput input">
                <input type="password" id="password-confirm" name="password-confirm" required placeholder=" ">
                <span class="label">
                    ${msg("passwordConfirm")}
                </span>
            </label>
            <div class="errors" id="password-confirm-error">
                <div class="error">
                    ${msg("passwordConfirmError")}
                </div>
            </div>

            <#if isAppInitiatedAction??>
            <div class="button validate" id="submit" >
                <span>${msg("doSubmit")}</span>
            </div>
            <button class="button validate" type="submit" name="cancel-aia" value="true" />
                <span>
                    ${msg("doCancel")}
                </span>
            </button>
            <#else>
            <div class="button validate" id="submit" >
                <span>${msg("doSubmit")}</span>
            </div>
            </#if>
        </form>
    <#elseif section = "script">
    <script>
        function passwordNewIsSet(password){
            return (password !== 'undefined' && password.trim() !== '')
        }

        function passwordConfirmIsSet(password){
            return (password !== 'undefined' && password.trim() !== '')
        }

        function validate(){
            const passwordNew = document.getElementById('password-new').value;
            const passwordConfirm = document.getElementById('password-confirm').value;
            if (!passwordNewIsSet(passwordNew)) {
                document.getElementById('password-error').style.display = "block";
                document.getElementById('password').value = '';
            }else{
                document.getElementById('password-error').style.display = "none";
            }

            if (!passwordConfirmIsSet(passwordConfirm)) {
                document.getElementById('password-confirm-error').style.display = "block";
                document.getElementById('password-confirm').value = '';
            }else{
                document.getElementById('password-confirm-error').style.display = "none";
            }

            if (passwordNewIsSet(passwordNew) &&
                passwordConfirmIsSet(passwordConfirm)) {
                document.getElementById('kc-passwd-update-form').submit();
            }
        }
        let button = document.getElementById('submit');
        button.addEventListener("click", validate, false);
        document.querySelectorAll('input').forEach( (input)=>{
            input.addEventListener('keypress', function (e) {
                var key = e.which || e.keyCode;
                if (key === 13) {
                    validate();
                }
            });
        });
    </script>
    </#if>
</@layout.registrationLayout>