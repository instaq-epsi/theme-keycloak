<#import "template.ftl" as layout>
<@layout.registrationLayout displayInfo=true; section>
    <#if section = "form">
        <form id="kc-reset-password-form" class="form" action="${url.loginAction}" method="post">
            <label class="prettyinput input">
                <input type="text" name="username" id="username" required placeholder=" ">
                <span class="label">
                    <#if !realm.loginWithEmailAllowed>${msg("username")}
                    <#elseif !realm.registrationEmailAsUsername>${msg("usernameOrEmail")}
                    <#else>${msg("email")}</#if>
                </span>
            </label>
            <div class="errors" id="username-error">
                <div class="error">
                    <#if !realm.loginWithEmailAllowed>${msg("usernameError")}
                    <#elseif !realm.registrationEmailAsUsername>${msg("usernameOrEmailError")}
                    <#else>${msg("emailError")}</#if>
                </div>
            </div>
            <div  class="account-options">
                <span><a href="${url.loginUrl}">${kcSanitize(msg("backToLogin"))?no_esc}</a></span>
            </div>

            <div class="button validate" id="submit">
                <span>${msg("doSubmit")}</span>
            </div>
        </form>
    <#elseif section = "script">
        <script>
            function emailIsSet(email){
                var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(String(email).toLowerCase());
            }

            function usernameIsSet(username){
                return (username !== 'undefined' && username.trim() !== '')
            }

            function validate(){
                const username = document.getElementById('username').value;
                <#if !realm.loginWithEmailAllowed>
                if(!emailIsSet(username)){
                    document.getElementById('username-error').style.display = "block";
                    document.getElementById('username').value = '';
                }else{
                    document.getElementById('username-error').style.display = "none";
                    document.getElementById('kc-reset-password-form').submit();
                }
                <#elseif !realm.registrationEmailAsUsername>
                if(!emailIsSet(username) && !usernameIsSet(username)){
                    document.getElementById('username-error').style.display = "block";
                    document.getElementById('username').value = '';
                }else{
                    document.getElementById('username-error').style.display = "none";
                    document.getElementById('kc-reset-password-form').submit();
                }
                <#else>
                if(!usernameIsSet(username)){
                    document.getElementById('username-error').style.display = "block";
                    document.getElementById('username').value = '';
                }else{
                    document.getElementById('username-error').style.display = "none";
                    document.getElementById('kc-reset-password-form').submit();
                }
                </#if>
            }

            let button = document.getElementById('submit');
            button.addEventListener("click", validate, false);
            document.querySelectorAll('input').forEach( (input)=>{
                input.addEventListener('keypress', function (e) {
                    var key = e.which || e.keyCode;
                    if (key === 13) {
                        validate();
                    }
                });
            });
        </script>
    <#elseif section = "info" >
        <p class="message-info">${kcSanitize(msg("emailInstruction"))?no_esc}</p>
    </#if>
</@layout.registrationLayout>