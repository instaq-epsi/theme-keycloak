<#import "template.ftl" as layout>
<@layout.registrationLayout; section>
    <#if section = "title">
        ${msg("updatePhoneNumberTitle", realm.name)}
    <#elseif section = "form">
        <form id="kc-totp-login-form" class="form" action="${url.loginAction}" method="post">
            <label class="prettyinput input">
                <input type="text" name="mobile_number" id="mobileNumber" required placeholder=" " autocomplete="mobile tel" aria-describedby="mobileNumber-hint">
                <span class="label">
                    ${msg("phoneNumber")}
                </span>
            </label>
            <div class="errors" id="phone-error">
                <div class="error">
                    ${msg("phoneNumberError")}
                </div>
            </div>
            <div class="button validate" id="submit" >
                <span>${msg("doSubmit")}</span>
            </div>
        </form>
        <p class="message-info">${msg("updatePhoneNumberMessage")}</p>
    <#elseif section = "script">
        <script>
            function phoneIsSet(phone){
                return (phone !== 'undefined' && phone.trim() !== '')
            }

            function validate(){
                const phone = document.getElementById('mobileNumber').value;
                if(!phoneIsSet(phone)){
                    document.getElementById('phone-error').style.display = "block";
                    document.getElementById('mobileNumber').value = '';
                }else{
                    document.getElementById('phone-error').style.display = "none";
                    document.getElementById('kc-totp-login-form').submit();
                }
            }
            let button = document.getElementById('submit');
            button.addEventListener("click", validate, false);
            document.querySelectorAll('input').forEach( (input)=>{
                input.addEventListener('keypress', function (e) {
                    var key = e.which || e.keyCode;
                    if (key === 13) {
                        validate();
                    }
                });
            });
        </script>
    </#if>
</@layout.registrationLayout>
