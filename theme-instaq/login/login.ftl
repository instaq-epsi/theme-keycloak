<#import "template.ftl" as layout>
<@layout.registrationLayout displayInfo=social.displayInfo; section>
    <#if section = "title">
        ${msg("loginTitle",(realm.displayName!''))}
    <#elseif section = "header">
        <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet"/>
        <link href="${url.resourcesPath}/img/favicon.png" rel="icon"/>
    <#elseif section = "topform">
        <h1 class="title" >${msg("loginTitle")}</h1>
    <#elseif section = "form">
        <#if realm.password>
            <div>
               <form id="kc-form-login" class="form" onsubmit="login.disabled = true; return true;" action="${url.loginAction}" method="post">
                    <label class="prettyinput input">
                        <input type="text" name="username" id="username" required placeholder=" "  >
                        <span class="label">
                            <#if !realm.loginWithEmailAllowed>${msg("username")}
                            <#elseif !realm.registrationEmailAsUsername>${msg("usernameOrEmail")}
                            <#else>${msg("email")}</#if>
                        </span>
                    </label>
                    <div class="errors" id="username-error">
                        <div class="error">
                            <#if !realm.loginWithEmailAllowed>${msg("usernameError")}
                            <#elseif !realm.registrationEmailAsUsername>${msg("usernameOrEmailError")}
                            <#else>${msg("emailError")}</#if>
                        </div>
                    </div>

                    <label class="prettyinput input">
                        <input type="password" id="password" name="password" required placeholder=" "  >
                        <span class="label">
                            ${msg("password")}
                        </span>
                    </label>
                    <div class="errors" id="password-error">
                        <div class="error">
                            ${msg("passwordError")}
                        </div>
                    </div>

                    <div class="account-options">
                        <#if realm.registrationAllowed && !usernameEditDisabled??>
                            <span class="registration">
                                <a href="${url.registrationUrl}">${msg("doRegister")}</a>
                            </span>
                        </#if>
                        <#if realm.resetPasswordAllowed>
                            <span class="forgot-password">
                                <a href="${url.loginResetCredentialsUrl}">${msg("doForgotPassword")}</a>
                            </span>
                        </#if>
                    </div>

                    <div class="button validate" id="submit" >
                        <span>${msg("doLogIn")}</span>
                    </div>
                </form>
            <#if social.providers??>
                <p class="para">${msg("selectAlternative")}</p>
                <div id="social-providers">
                    <#list social.providers as p>
                    <input class="social-link-style" type="button" onclick="location.href='${p.loginUrl}';" value="${p.displayName}"/>
                    </#list>
                </div>
            </#if>
        </#if>
    <#elseif section = "script">
        <script>
            function emailIsSet(email){
                var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(String(email).toLowerCase());
            }

            function usernameIsSet(username){
                return (username !== 'undefined' && username.trim() !== '')
            }

            function passwordIsSet(password){
                return (password !== 'undefined' && password.trim() !== '')
            }

            function validate(){
                const username = document.getElementById('username').value;
                const password = document.getElementById('password').value;
                <#if !realm.loginWithEmailAllowed>
                if(!emailIsSet(username)){
                    document.getElementById('username-error').style.display = "block";
                    document.getElementById('username').value = '';
                }else{
                    document.getElementById('username-error').style.display = "none";
                }
                <#elseif !realm.registrationEmailAsUsername>
                if(!emailIsSet(username) && !usernameIsSet(username)){
                    document.getElementById('username-error').style.display = "block";
                    document.getElementById('username').value = '';
                }else{
                    document.getElementById('username-error').style.display = "none";
                }
                <#else>
                if(!usernameIsSet(username)){
                    document.getElementById('username-error').style.display = "block";
                    document.getElementById('username').value = '';
                }else{
                    document.getElementById('username-error').style.display = "none";
                }
                </#if>
                if (!passwordIsSet(password)) {
                    document.getElementById('password-error').style.display = "block";
                    document.getElementById('password').value = '';
                }else{
                    document.getElementById('password-error').style.display = "none";
                }
                if (passwordIsSet(password) &&
                    <#if !realm.loginWithEmailAllowed>
                    emailIsSet(username)
                    <#elseif !realm.registrationEmailAsUsername>
                    (emailIsSet(username) ||
                    usernameIsSet(username))
                    <#else>
                    usernameIsSet(username)
                    </#if>
                    ) {
                    document.getElementById('kc-form-login').submit();
                }
            }
            let button = document.getElementById('submit');
            button.addEventListener("click", validate, false);
            document.querySelectorAll('input').forEach( (input)=>{
                input.addEventListener('keypress', function (e) {
                    var key = e.which || e.keyCode;
                    if (key === 13) {
                        validate();
                    }
                });
            });
        </script>
    </#if>
</@layout.registrationLayout>
