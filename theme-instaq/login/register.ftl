<#import "template.ftl" as layout>
<@layout.registrationLayout; section>
    <#if section = "title">
        ${msg("registerWithTitle",(realm.displayName!''))}
    <#elseif section = "header">
        <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet"/>
        <link href="${url.resourcesPath}/img/favicon.png" rel="icon"/>
    <#elseif section = "topform">
        <h1 class="title" >${msg("registerWithTitle")}</h1>
    <#elseif section = "form">
        <form id="kc-register-form" class="form" action="${url.registrationAction}" method="post">

            <label class="prettyinput input">
                <input type="text" name="firstName" id="firstName" required placeholder=" ">
                <span class="label">
                    ${msg("firstName")}
                </span>
            </label>
            <div class="errors" id="firstName-error">
                <div class="error">
                    ${msg("firstNameError")}
                </div>
            </div>

            <label class="prettyinput input">
                <input type="text" name="lastName" id="lastName" required placeholder=" ">
                <span class="label">
                    ${msg("lastName")}
                </span>
            </label>
            <div class="errors" id="lastName-error">
                <div class="error">
                    ${msg("lastNameError")}
                </div>
            </div>

            <label class="prettyinput input">
                <input type="text" name="email" id="email" required placeholder=" ">
                <span class="label">
                    ${msg("email")}
                </span>
            </label>
            <div class="errors" id="email-error">
                <div class="error">
                    ${msg("emailError")}
                </div>
            </div>

        <#if !realm.registrationEmailAsUsername>
            <label class="prettyinput input">
                <input type="text" name="username" id="username" required placeholder=" ">
                <span class="label">
                    ${msg("username")}
                </span>
            </label>
            <div class="errors" id="username-error">
                <div class="error">
                    ${msg("usernameError")}
                </div>
            </div>
        </#if>

        <#if passwordRequired>
            <label class="prettyinput input">
                <input type="password" name="password" id="password" required placeholder=" " autocomplete="new-password">
                <span class="label">
                    ${msg("password")}
                </span>
            </label>
            <div class="errors" id="password-error">
                <div class="error">
                    ${msg("passwordError")}
                </div>
            </div>
            <label class="prettyinput input">
                <input type="password" name="password-confirm" id="password-confirm" required placeholder=" ">
                <span class="label">
                    ${msg("passwordConfirm")}
                </span>
            </label>
            <div class="errors" id="password-confirm-error">
                <div class="error">
                    ${msg("passwordConfirmError")}
                </div>
            </div>
        </#if>

        <#if recaptchaRequired??>
            <div class="g-recaptcha" data-size="compact" data-sitekey="${recaptchaSiteKey}"></div>
        </#if>
            <span><a href="${url.loginUrl}">${kcSanitize(msg("backToLogin"))?no_esc}</a></span>
            <div class="button validate" id="submit" tabindex="3">
                <span>${msg("doRegister")}</span>
            </div>
        </form>
    <#elseif section = "script">
        <script>
            function firstNameIsSet(fistName){
                return (fistName !== 'undefined' && fistName.trim() !== '')
            }

            function lastNameIsSet(lastName){
                return (lastName !== 'undefined' && lastName.trim() !== '')
            }

            function emailIsSet(email){
                var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(String(email).toLowerCase());
            }

            function usernameIsSet(username){
                return (username !== 'undefined' && username.trim() !== '')
            }

            function passwordIsSet(password){
                return (password !== 'undefined' && password.trim() !== '')
            }

            function passwordConfirmIsSet(password){
                return (password !== 'undefined' && password.trim() !== '')
            }

            function validate(){
                const firstName = document.getElementById('firstName').value;
                const lastName = document.getElementById('lastName').value;
                const email = document.getElementById('email').value;
                const password = document.getElementById('password').value;
                const passwordConfirm = document.getElementById('password-confirm').value;
                <#if !realm.registrationEmailAsUsername>
                const username = document.getElementById('username').value;
                </#if>
                if (!firstNameIsSet(firstName)) {
                    document.getElementById('firstName-error').style.display = "block";
                    document.getElementById('firstName').value = '';
                }else{
                    document.getElementById('firstName-error').style.display = "none";
                }

                if (!lastNameIsSet(lastName)) {
                    document.getElementById('lastName-error').style.display = "block";
                    document.getElementById('lastName').value = '';
                }else{
                    document.getElementById('lastName-error').style.display = "none";
                }

                if (!emailIsSet(email)) {
                    document.getElementById('email-error').style.display = "block";
                    document.getElementById('email').value = '';
                }else{
                    document.getElementById('email-error').style.display = "none";
                }
                <#if !realm.registrationEmailAsUsername>
                if (!usernameIsSet(username)) {
                    document.getElementById('username-error').style.display = "block";
                    document.getElementById('username').value = '';
                }else{
                    document.getElementById('username-error').style.display = "none";
                }
                </#if>
                if (!passwordIsSet(password)) {
                    document.getElementById('password-error').style.display = "block";
                    document.getElementById('password').value = '';
                }else{
                    document.getElementById('password-error').style.display = "none";
                }

                if (!passwordConfirmIsSet(passwordConfirm)) {
                    document.getElementById('password-confirm-error').style.display = "block";
                    document.getElementById('password-confirm').value = '';
                }else{
                    document.getElementById('password-confirm-error').style.display = "none";
                }
                if (passwordIsSet(password) &&
                    <#if !realm.registrationEmailAsUsername>
                    usernameIsSet(username) &&
                    </#if>
                    firstNameIsSet(firstName) &&
                    lastNameIsSet(firstName) &&
                    emailIsSet(email) &&
                    passwordConfirmIsSet(passwordConfirm)) {
                    document.getElementById('kc-register-form').submit();
                }
            }
            let button = document.getElementById('submit');
            button.addEventListener("click", validate, false);
            document.querySelectorAll('input').forEach( (input)=>{
                input.addEventListener('keypress', function (e) {
                    var key = e.which || e.keyCode;
                    if (key === 13) {
                        validate();
                    }
                });
            });
        </script>
    </#if>
</@layout.registrationLayout>