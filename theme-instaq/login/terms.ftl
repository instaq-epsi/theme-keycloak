<#import "template.ftl" as layout>
<@layout.registrationLayout displayMessage=false; section>
    <#if section = "header">
        ${msg("termsTitle")}
    <#elseif section = "form">
    <div id="kc-terms-text">
        <p class="message-info">${kcSanitize(msg("termsText"))?no_esc}</p>
    </div>
    <form class="form" action="${url.loginAction}" method="POST">
        <input class="button validate" name="accept" id="kc-accept" type="submit" value="${msg("doAccept")}">
        <input class="button validate" name="cancel" id="kc-decline" type="submit" value="${msg("doDecline")}">
    </form>
    <div class="clearfix"></div>
    </#if>
</@layout.registrationLayout>