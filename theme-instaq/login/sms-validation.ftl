<#import "template.ftl" as layout>
<@layout.registrationLayout ; section>
    <#if section = "form">
        <form id="kc-totp-login-form" class="form" action="${url.loginAction}" method="post">
            <label class="prettyinput input">
                    <input id="code" name="smsCode" type="number" required placeholder=" ">
                    <span class="label">
                        ${msg("sms-auth.code")}
                    </span>
                </label>
                <div class="errors" id="code-error">
                    <div class="error">
                        ${msg("smsAuthCodeError")}
                    </div>
                </div>
                <div class="button validate" id="submit" >
                    <span>${msg("doSubmit")}</span>
                </div>
        </form>
        <p class="message-info">${msg("sms-auth.instruction")}</p>
        <#if client?? && client.baseUrl?has_content>
            <p><a id="backToApplication" href="${client.baseUrl}">${msg("backToApplication")}</a></p>
        </#if>
    <#elseif section = "script">
        <script>
            function codeIsSet(code){
                return (code !== 'undefined' && code.trim() !== '')
            }

            function validate(){
                const code = document.getElementById('code').value;
                if(!codeIsSet(code)){
                    document.getElementById('code-error').style.display = "block";
                    document.getElementById('code').value = '';
                }else{
                    document.getElementById('code-error').style.display = "none";
                    document.getElementById('kc-totp-login-form').submit();
                }
            }
            let button = document.getElementById('submit');
            button.addEventListener("click", validate, false);
            document.querySelectorAll('input').forEach( (input)=>{
                input.addEventListener('keypress', function (e) {
                    var key = e.which || e.keyCode;
                    if (key === 13) {
                        validate();
                    }
                });
            });
        </script>
    </#if>
</@layout.registrationLayout>
